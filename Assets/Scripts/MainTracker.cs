﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using UnityEngine.UI;

public class MainTracker : MonoBehaviour , ITrackableEventHandler
{

    protected TrackableBehaviour trackable;
    protected TrackableBehaviour.Status m_PreviousStatus;
    protected TrackableBehaviour.Status m_NewStatus;

    public bool mainTracked = false;

    private bool oTracked = false;
    private bool hTracked = false;

    public Canvas canvas;
    public Text middleText;
    public GameObject HandTracker;
    public GameObject ObjectTracker;


    // Start is called before the first frame update
    void Start()
    {
        trackable = GetComponent<TrackableBehaviour>();
        if (trackable)
            trackable.RegisterTrackableEventHandler(this);


        canvas.GetComponent<Text>().text = "test";
    }

    // Update is called once per frame
    void Update()
    {


        middleText.GetComponent<Text>().text = "2000";

        //mainTracked = GetComponent<MainTracker>().mainTracked;
        hTracked = HandTracker.GetComponent<HandTracker>().handTracked;
        oTracked = ObjectTracker.GetComponent<ObjectTracker>().objectTracked;

        canvas.GetComponent<Text>().text = ("Main: " + mainTracked + " Hand: " + hTracked + " Object: " + oTracked);


        if (mainTracked == true)
        {
            
        }
        

    }

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        m_PreviousStatus = previousStatus;
        m_NewStatus = newStatus;


        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            TrackingFound();
        }
        else if (previousStatus == TrackableBehaviour.Status.TRACKED &&
                newStatus == TrackableBehaviour.Status.NO_POSE)
        {
            TrackingLost();
        }
        else
        {
            // For combo of previousStatus=UNKNOWN + newStatus=UNKNOWN|NOT_FOUND
            // Vuforia is starting, but tracking has not been lost or found yet
            // Call OnTrackingLost() to hide the augmentations
            TrackingLost();
        }


    }

    private void TrackingFound()
    {
        mainTracked = true;

        var rendererComponents = trackable.GetComponentsInChildren<Renderer>(true);
        foreach (var component in rendererComponents)
            component.enabled = true;

        Debug.Log("MainTracker Found");
    }

    private void TrackingLost()
    {
        mainTracked = false;

        var rendererComponents = trackable.GetComponentsInChildren<Renderer>(true);
        foreach (var component in rendererComponents)
            component.enabled = false;

        Debug.Log("MainTracker Lost");

    }


}

