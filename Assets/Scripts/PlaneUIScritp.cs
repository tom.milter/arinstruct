﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlaneUIScritp : MonoBehaviour
{

    public Text trackedUI;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 uiPosition = Camera.main.WorldToScreenPoint(this.transform.position);
        trackedUI.transform.position = uiPosition;
        
    }
}
