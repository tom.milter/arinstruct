﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CableScript : MonoBehaviour
{

    LineRenderer line;

    public Vector3 Anfang;
    public Vector3 Ziel;




    // Start is called before the first frame update
    void Start()
    {
        line = this.GetComponent<LineRenderer>();
        line.SetPosition(0, Anfang);
        line.SetPosition(1, Ziel);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
