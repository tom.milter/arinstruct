﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CablesDestructor : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameObject[] cables = GameObject.FindGameObjectsWithTag("Cable");
        foreach (GameObject ca in cables)
        {
            GameObject.Destroy(ca);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
