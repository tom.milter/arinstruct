﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using TMPro;





public class TCPTestServer : MonoBehaviour
{

    public GameObject Ser0;
    public GameObject Ser1;
    public GameObject Ser2;
    public GameObject Ser3;
    public GameObject Ser4;
    public GameObject Ser5;
    public GameObject Ser6;
    public GameObject Ser7;

   public MenuScript Menu;

    public BreadboardScript Breadboard;

    public TextMeshPro ResText;
    public RegisterAnzeige RS;





    #region private members 	
    /// <summary> 	
    /// TCPListener to listen for incomming TCP connection 	
    /// requests. 	
    /// </summary> 	
    private TcpListener tcpListener;
    /// <summary> 
    /// Background thread for TcpServer workload. 	
    /// </summary> 	
    private Thread tcpListenerThread;
    /// <summary> 	
    /// Create handle to connected tcp client. 	
    /// </summary> 	
    private TcpClient connectedTcpClient;
    

    private TextMeshPro text0;
    private TextMeshPro text1;
    private TextMeshPro text2;
    private TextMeshPro text3;
    private TextMeshPro text4;
    private TextMeshPro text5;
    private TextMeshPro text6;
    private TextMeshPro text7;
    #endregion

    private bool ChangeUI;
    private string newMessage;

    // Use this for initialization
    void Start()
    {

        // Start TcpServer background thread 		
        tcpListenerThread = new Thread(new ThreadStart(ListenForIncommingRequests));
        tcpListenerThread.IsBackground = true;
        tcpListenerThread.Start();

        // Get The Text on the Serial Monitor
        text0 = Ser0.GetComponent<TextMeshPro>();
        text1 = Ser1.GetComponent<TextMeshPro>();
        text2 = Ser2.GetComponent<TextMeshPro>();
        text3 = Ser3.GetComponent<TextMeshPro>();
        text4 = Ser4.GetComponent<TextMeshPro>();
        text5 = Ser5.GetComponent<TextMeshPro>();
        text6 = Ser6.GetComponent<TextMeshPro>();
        text7 = Ser7.GetComponent<TextMeshPro>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            SendMessage();
        }

        //Text aufrücken wenn neue Nachricht angekommen ist
        if (ChangeUI == true)
        {
            text7.text = text6.text;
            text6.text = text5.text;
            text5.text = text4.text;
            text4.text = text3.text;
            text3.text = text2.text;
            text2.text = text1.text;
            text1.text = text0.text;
            text0.text = newMessage;
            ChangeUI = false;
        }
    }

    /// <summary> 	
    /// Runs in background TcpServerThread; Handles incomming TcpClient requests 	
    /// </summary> 	
    private void ListenForIncommingRequests()
    {
        //Try to Start a TCP Listen Server (copy from TCP Example from Unity)
        try
        {
            // Create listener		
            tcpListener = new TcpListener(IPAddress.Any, 5560);
            tcpListener.Start();
            Debug.Log("Server is listening");
            Byte[] bytes = new Byte[1024];
            while (true)
            {
                using (connectedTcpClient = tcpListener.AcceptTcpClient())
                {
                    // Get a stream object for reading 					
                    using (NetworkStream stream = connectedTcpClient.GetStream())
                    {
                        int length;
                        // Read incomming stream into byte arrary. 						
                        while ((length = stream.Read(bytes, 0, bytes.Length)) != 0)
                        {
                            var incommingData = new byte[length];
                            Array.Copy(bytes, 0, incommingData, 0, length);
                            // Convert byte array to string message. 							
                            string clientMessage = Encoding.ASCII.GetString(incommingData);
                            Debug.Log("client message received as: " + clientMessage);


                            ///////////////////////////////////////////////////////
                            // My Code
                            string[] command = clientMessage.Split(' ');
                            if (command[0] == "SERIAL")
                            {
                                SendtoUI(command[1]);
                                if (command[1] == "Button")
                                {
                                    Breadboard.Level1Button = true;
                                }
                                if (command[1] == "LEDConnected")
                                {
                                    Breadboard.Level2LED = true;
                                }
                                if (command[1] == "RGBConnected")
                                {
                                    Breadboard.Level3LED = true;
                                }
                                if (command[1] == "RES")
                                {
                                    Breadboard.ResMessure = true;
                                    ResText.text = command[2];
                                }
                                if (command[1] == "RegisterStep")
                                {
                                    RS.ChangeAnzeige(int.Parse(command[2]));
                                }

                            }
                            if (command[0] == "Bup")
                            {
                                Debug.Log("UP");
                                if (Breadboard.Level == 0)
                                {
                                    Breadboard.Level0BUp = true;
                                }else
                                {
                                    Menu.ButtonUp();
                                }
                                
                            }
                            if (command[0] == "Bdown")
                            {
                                Debug.Log("DOWN");
                                if (Breadboard.Level == 0)
                                {
                                    Breadboard.Level0BDown = true;
                                }
                                else
                                {
                                    Menu.ButtonDown();
                                }
                            }
                            if (command[0] == "Benter")
                            {
                                Debug.Log("ENTER");
                                if (Breadboard.Level == 0)
                                {
                                    Breadboard.Level0Enter = true;
                                }
                                else
                                {
                                    Menu.ButtonEnter();
                                }
                            }


                            ////////////////////////////////////////////////////////////////
                        }
                    }
                }
            }
        }
        catch (SocketException socketException)
        {
            Debug.Log("SocketException " + socketException.ToString());
        }
    }
    /// <summary> 	
    /// Send message to client using socket connection. 	
    /// </summary> 	
    private void SendMessage()
    {
        if (connectedTcpClient == null)
        {
            return;
        }

        try
        {
            // Get a stream object for writing. 			
            NetworkStream stream = connectedTcpClient.GetStream();
            if (stream.CanWrite)
            {
                string serverMessage = "This is a message from your server.";
                // Convert string message to byte array.                 
                byte[] serverMessageAsByteArray = Encoding.ASCII.GetBytes(serverMessage);
                // Write byte array to socketConnection stream.               
                stream.Write(serverMessageAsByteArray, 0, serverMessageAsByteArray.Length);
                Debug.Log("Server sent his message - should be received by client");
            }
        }
        catch (SocketException socketException)
        {
            Debug.Log("Socket exception: " + socketException);
        }
    }







    private void SendtoUI(string comment)
    {
        //alle Textelemente aufrücken
        ChangeUI = true;
        // Nachricht in globale schreiben
        newMessage = comment;
        
    }
}
