﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutlineScript : MonoBehaviour
{

    public Vector3 distance;
    public Transform HandImageTarget;

    private bool outlined = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 position = HandImageTarget.transform.position;
        position[2] = position[2] * 0.3f; // Minimiere die Y Variable, da der Abstand mehr auf seitwärts ankommt.

        distance = this.transform.position - position;

        if (distance.magnitude < 5)
        {
            if (outlined == false)
            {
                this.GetComponent<Outline>().enabled = true;
                outlined = true;
            }
        }
        else if (outlined == true)
        {
            this.GetComponent<Outline>().enabled = false;
            outlined = false;
        }
    }
}
