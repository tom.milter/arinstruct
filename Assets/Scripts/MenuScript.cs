﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MenuScript : MonoBehaviour
{
    int lastIndex;
    int LastDepth;
    int index; // Position im Menü
    int depth; // Tiefe im Menü
               // 0 = Hauptmenü ( Start, StageAuswahl, Shutdown)
               // 1 = StageAuswahl (Button, LED, RGB, Widerstand messen, Photsensor, Regsiter1, Register2)
    int maxIndex;

    public BreadboardScript Breadboard;

    public TextMeshPro MenuText0;
    public TextMeshPro MenuText1;
    public TextMeshPro MenuText2;
    public TextMeshPro[] MenuTexts;

    public GameObject Button0;
    public GameObject Button1;
    public GameObject Button2;
    public GameObject[] Buttons;

    string[][] MenuTextArray = new string[2][];  //erste Position tiefe zweite  index

    AudioSource audio;


    // Start is called before the first frame update
    void Start()
    {
        Buttons = new GameObject[] { Button0, Button1, Button2 };
        MenuTexts = new TextMeshPro[] { MenuText0, MenuText1, MenuText2 };

        //Array mit Text erzeugen
        MenuTextArray[0] = new string[3] { "Skip", "Stage-Auswahl", "Herunterfahren" };
        MenuTextArray[1] = new string[9] { "Button", "LED", "RGB-LED", "Widerstand messen", "Lichtsensor", "Schieberegister", "Schieberegister Teil 2", " ","Zurück" };



        index = 0;
        lastIndex = 1;
        depth = 0;
        LastDepth = 1;
        maxIndex = 2;

        Button0.GetComponent<Renderer>().material.color = Color.grey;
        Button1.GetComponent<Renderer>().material.color = Color.red;
        Button2.GetComponent<Renderer>().material.color = Color.grey;

        audio = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {
        //MaterialUpdate (nur bei änderung)
        if (index != lastIndex)
        {
            ChangeText();
            //alten Button Grau färben
            int j = index;
            int k = lastIndex;
            if (index > 2) { j = j - 3; }
            if (index > 5) { j = j - 3; }
            if (lastIndex > 2) { k = k - 3; }
            if (lastIndex > 5) { k = k - 3; }
            Buttons[k].GetComponent<Renderer>().material.color = Color.grey;
            Debug.Log(index);
            //aktuellen Button Rot färben
            Buttons[j].GetComponent<Renderer>().material.color = Color.red;

            lastIndex = index;
        }

        //TextUpdate (nur bei änderung) 
        if (depth != LastDepth)
        {
            ChangeText();


            if (depth == 0)
            {
                maxIndex = 2;
            }else if (depth == 1)
            {
                maxIndex = 8;
            }

            LastDepth = depth;
        }
    }

    void ChangeText()
    {
        for (int i = 0; i <= 2; i++) // Nur 3 Texte immer geändert, bei depth 1 muss "gescrollt" werden.
        {
            int j = i;
            if (index > 2) { j = j + 3; }
            if (index > 5) { j = j + 3; }
            MenuTexts[i].text = MenuTextArray[depth][j];
        }
    }

    public void ButtonUp()
    {
        if (index >0)
        {
            index = index - 1;
        }
        else
        {
            index = maxIndex;
            
        }
            
    }

    public void ButtonDown()
    {
        if (index < maxIndex)
        {
            index= index + 1;
        }
        else
        {
            index = 0;
        }

    }

    public void ButtonEnter()
    {
        audio.Play();
        if (depth == 0)         //Hauptmenü
        {
            if (index == 0)
            {
                int lvl = Breadboard.Level;
                if (lvl == 0)
                {
                    Breadboard.Level1();
                }
                if (lvl == 1)
                {
                    Breadboard.Level2();
                }
                if (lvl == 2)
                {
                    Breadboard.Level3();
                }
                if (lvl == 3)
                {
                    Breadboard.Level4();
                }
                if (lvl == 4)
                {
                    Breadboard.Level5();
                }
                if (lvl == 5)
                {
                    Breadboard.Level6();
                }
                if (lvl == 6)
                {
                    Breadboard.Level7();
                }

            }
            if (index == 1)
            {
                depth = 1;
            }
            if (index ==2)
            {
                //Shutdown
                Application.Quit();
            }
        }
        if (depth == 1)
        {
            if (index == 0)
            {
                Breadboard.Level1();
            }
            if (index == 1)
            {
                Breadboard.Level2();
            }
            if (index == 2)
            {
                Breadboard.Level3();
            }
            if (index == 3)
            {
                Breadboard.Level4();
            }
            if (index == 4)
            {
                Breadboard.Level5();
            }
            if (index == 5)
            {
                Breadboard.Level6();
            }
            if (index == 6)
            {
                Breadboard.Level7();
            }
            if (index == 7)
            {

            }
            if (index == 8)
            {
                depth = 0;
                index = 0;
            }

        }
        
    

       
    }
}
