﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using UnityEngine.UI;

public class ObjectTracker : MonoBehaviour, ITrackableEventHandler
{

    protected TrackableBehaviour trackable;
    protected TrackableBehaviour.Status m_PreviousStatus;
    protected TrackableBehaviour.Status m_NewStatus;

    private bool mainTracked;
    private bool hTracked;
    public bool objectTracked = false;
    public GameObject HandTracker;
    public GameObject MainTracker;

    public Vector3 distance;
    private bool outlined = false;
    public Transform HandImageTarget;

    public GameObject MotorCover;


    // Start is called before the first frame update
    void Start()
    {
        trackable = GetComponent<TrackableBehaviour>();
        if (trackable)
            trackable.RegisterTrackableEventHandler(this);


        
    }



    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        m_PreviousStatus = previousStatus;
        m_NewStatus = newStatus;


        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            TrackingFound();
        }
        else if (previousStatus == TrackableBehaviour.Status.TRACKED &&
                newStatus == TrackableBehaviour.Status.NO_POSE)
        {
            TrackingLost();
        }
        else
        {
            // For combo of previousStatus=UNKNOWN + newStatus=UNKNOWN|NOT_FOUND
            // Vuforia is starting, but tracking has not been lost or found yet
            // Call OnTrackingLost() to hide the augmentations
            TrackingLost();
        }


    }

    private void TrackingFound()
    {
        objectTracked = true;

        var rendererComponents = trackable.GetComponentsInChildren<Renderer>(true);
        foreach(var component in rendererComponents)
                component.enabled = true;

        Debug.Log("Object Found");
    }

    private void TrackingLost()
    {
        objectTracked = false;

        var rendererComponents = trackable.GetComponentsInChildren<Renderer>(true);
        foreach (var component in rendererComponents)
            component.enabled = false;

        Debug.Log("Object Lost");

    }

    // Update is called once per frame
    void Update()
    {
        mainTracked = MainTracker.GetComponent<MainTracker>().mainTracked;
        hTracked = HandTracker.GetComponent<HandTracker>().handTracked;

        


        if (objectTracked == true)
        {


            if (hTracked == true)
            {
                Vector3 position = HandImageTarget.transform.position;
                position[2] = position[2]*0.3f; // Minimiere die Y Variable, da der Abstand mehr auf seitwärts ankommt.

                distance = this.transform.position - position;

                if (distance.magnitude < 5)
                {
                    if (outlined == false)
                    {
                        MotorCover.GetComponent<Outline>().enabled = true;
                        outlined = true;
                    }
                }else if (outlined == true)
                {
                    MotorCover.GetComponent<Outline>().enabled = false;
                    outlined = false;
                }
            }
        }


    }


}
