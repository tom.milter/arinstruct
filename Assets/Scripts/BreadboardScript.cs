﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class BreadboardScript : MonoBehaviour, ITrackableEventHandler
{
    protected TrackableBehaviour mTrackableBehaviour;
    protected TrackableBehaviour.Status m_PreviousStatus;
    protected TrackableBehaviour.Status m_NewStatus;

    public LinesScript LinesScript;
    public string BreadboardTrackedStatus;

    public int Level = 0; //aktuelles "Level"

    //checks ob Level Erledigt, global vom TCP Server setzbar
    public bool Level0BUp = false;
    public bool Level0BDown = false;
    public bool Level0Enter = false;

    public bool Level1Button = false;

    public bool Level2LED = false;

    public bool Level3LED = false;

    public bool ResMessure = false;

    public CheckListTracker Checklist;

    public GameObject[] EmptyLevels;

    public GameObject EmptyDauerbrenner;

    public GameObject EmptyLevel0;
    public GameObject EmptyLevel1;
    public GameObject EmptyLevel2;
    public GameObject EmptyLevel3;
    public GameObject EmptyLevel4;
    public GameObject EmptyLevel5;
    public GameObject EmptyLevel6;
    public GameObject EmptyLevel7;

    //audio 
    AudioSource audio;


    // Start is called before the first frame update
    void Start()
    {
        EmptyLevels = new GameObject[] { EmptyLevel0, EmptyLevel1, EmptyLevel2, EmptyLevel3, EmptyLevel4, EmptyLevel5, EmptyLevel6, EmptyLevel7 };

        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
           
        
        // Debugger , choose Level: (default 0)
        Level0();
        audio = GetComponent<AudioSource>();
    }
    protected virtual void OnDestroy()
    {
        if (mTrackableBehaviour)
            mTrackableBehaviour.UnregisterTrackableEventHandler(this);
    }

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        m_PreviousStatus = previousStatus;
        m_NewStatus = newStatus;

        Debug.Log("Trackable " + mTrackableBehaviour.TrackableName +
                  " " + mTrackableBehaviour.CurrentStatus +
                  " -- " + mTrackableBehaviour.CurrentStatusInfo);

        BreadboardTrackedStatus =  mTrackableBehaviour.CurrentStatus.ToString();

        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            OnTrackingFound();
        }
        else if (previousStatus == TrackableBehaviour.Status.TRACKED &&
                 newStatus == TrackableBehaviour.Status.NO_POSE)
        {
            OnTrackingLost();
        }
        else
        {
            // For combo of previousStatus=UNKNOWN + newStatus=UNKNOWN|NOT_FOUND
            // Vuforia is starting, but tracking has not been lost or found yet
            // Call OnTrackingLost() to hide the augmentations
            OnTrackingLost();
        }
    }

    protected virtual void OnTrackingFound()
    {
        if (mTrackableBehaviour)
        {
            var rendererComponents =EmptyLevels[Level].GetComponentsInChildren<Renderer>(true);             // macht nur das passende Level Sichtbar
            //var rendererComponents = mTrackableBehaviour.GetComponentsInChildren<Renderer>(true);         //macht alles sichtnar (zum debuggen /tracker plazieren)

            // Enable rendering:
            foreach (var component in rendererComponents)
                component.enabled = true;



            rendererComponents = EmptyDauerbrenner.GetComponentsInChildren<Renderer>(true);

            foreach (var component in rendererComponents)
                component.enabled = true;

        }
    }


    protected virtual void OnTrackingLost()
    {
        if (mTrackableBehaviour)
        {
            var rendererComponents = mTrackableBehaviour.GetComponentsInChildren<Renderer>(true);

            // Disable rendering:
            foreach (var component in rendererComponents)
                component.enabled = false;


        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Level == 0)
        {
            if (Level0BDown == true && Level0BUp == true && Level0Enter == true)
            {
                Level1();
                audio.Play();
            }
        }
        if (Level == 1)
        {
            if (Level1Button == true)
            {
                audio.Play();
                Level2();
            }
        }
        if (Level == 2)
        {
            if (Level2LED == true)
            {
                Level4(); // zu Level 4, da RGB LED nicht geht!!
                audio.Play();
            }
        }
        if (Level == 3)
        {
            if (Level3LED == true)
            {
                Level4();
                audio.Play();
            }
        }
        if (Level == 4)
        {
            if (ResMessure == true)
            {
                Level5();
                audio.Play();
            }
        }

    }

    void DestroyCables()        // Alle Kabel löschen
    {
        
            
    }



    public void Level0()           //Buttons am Raspberry Setuppen (autostart)
    {
        Level = 0;
        Checklist.changeText(0);
        OnTrackingLost();
        OnTrackingFound();

        LinesScript.MakeLine(new Vector3(0.191f, 0, 0.7f), new Vector3(0.191f, 0, 0.55f), Color.red, EmptyLevels[Level].transform);
        LinesScript.MakeLine(new Vector3(0.041f, 0, 0.7f), new Vector3(0.041f, 0, 0.55f), Color.red, EmptyLevels[Level].transform);
        LinesScript.MakeLine(new Vector3(-0.105f, 0, 0.7f), new Vector3(-0.105f, 0, 0.55f), Color.red, EmptyLevels[Level].transform);

        LinesScript.MakeLine(new Vector3(0.6f, 0, 0.85f), new Vector3(0.1293f, 0, 0.55f), Color.yellow, EmptyLevels[Level].transform);
        LinesScript.MakeLine(new Vector3(0.55f, 0, 0.85f), new Vector3(-0.0131f, 0, 0.55f), Color.red, EmptyLevels[Level].transform);
        LinesScript.MakeLine(new Vector3(0.5f, 0, 0.85f), new Vector3(-.1586f, 0, 0.55f), Color.green, EmptyLevels[Level].transform);

    }


    public void Level1()       // Button
    {
        Level = 1;
        Checklist.changeText(Level);
        Checklist.t7.text = "Checkliste";
        OnTrackingLost();
        OnTrackingFound();


        LinesScript.MakeLine(new Vector3(1.6417f, 0, -0.2345f), new Vector3(1.774f, 0, 0.853f), Color.green, EmptyLevels[Level].transform);  // Ground Cable
        LinesScript.MakeLine(new Vector3(1.5868f, 0, -0.1109f), new Vector3(1.5868f, 0, -0.0059f),Color.grey, EmptyLevels[Level].transform); // Arduino Pin 11


    }
    public void Level2()           //LED
    {
        Level = 2;
        Checklist.changeText(Level);
        OnTrackingLost();
        OnTrackingFound();

        LinesScript.MakeLine(new Vector3(1.16f, 0, -0.12f), new Vector3(1.69f, 0, 0.853f), Color.green, EmptyLevels[Level].transform);
        
    }
    public void Level3()           //RGB LED
    {
        Level = 3;
        Checklist.changeText(Level);
        OnTrackingLost();
        OnTrackingFound();

        LinesScript.MakeLine(new Vector3(0.955f, 0, -0.171f), new Vector3(1.6f, 0, 0.853f), Color.red, EmptyLevels[Level].transform);
        LinesScript.MakeLine(new Vector3(0.89f, 0, -0.171f), new Vector3(1.55f, 0, 0.853f), Color.green, EmptyLevels[Level].transform);
        LinesScript.MakeLine(new Vector3(0.857f, 0, -0.171f), new Vector3(1.5f, 0, 0.853f), Color.blue, EmptyLevels[Level].transform);

        LinesScript.MakeLine(new Vector3(0.91f, 0, 0f), new Vector3(0.91f, 0, -0.379f), Color.black, EmptyLevels[Level].transform);

    }
    public void Level4()           //Widerstand Messen
    {
        Level = 4;
        Checklist.changeText(Level);
        OnTrackingLost();
        OnTrackingFound();

        LinesScript.MakeLine(new Vector3(0.56f, 0, -0.374f), new Vector3(0.56f, 0, 0f), Color.black, EmptyLevels[Level].transform);
        LinesScript.MakeLine(new Vector3(0.386f, 0, -0.374f), new Vector3(1.313f, 0, 1.446f), Color.green, EmptyLevels[Level].transform);
        LinesScript.MakeLine(new Vector3(0.216f, 0, -0.374f), new Vector3(0.216f, 0, -0.06f), Color.red, EmptyLevels[Level].transform);
    }
    public void Level5()           //Lichtsensor
    {
        Level = 5;
        Checklist.changeText(Level);
        OnTrackingLost();
        OnTrackingFound();
    }
    public void Level6()           //Schieberegister
    {
        Level = 6;
        Checklist.changeText(Level);
        OnTrackingLost();
        OnTrackingFound();

        //LED Line 0
        LinesScript.MakeLine(new Vector3(0.27f, 0, -0.912f), new Vector3(0.884f, 0, -1.134f), Color.green, EmptyLevels[Level].transform);
        //GND
        LinesScript.MakeLine(new Vector3(0.471f, 0, -1.18f), new Vector3(0.471f, 0, -1.25f), Color.black, EmptyLevels[Level].transform);
        // Enable
        LinesScript.MakeLine(new Vector3(0.331f, 0, -0.895f), new Vector3(0.331f, 0, -0.5785f), Color.blue, EmptyLevels[Level].transform);


        //Cables von L to R
        LinesScript.MakeLine(new Vector3(0.306f, 0, -0.8441f), new Vector3(1.03f, 0, 1.07f), Color.blue, EmptyLevels[Level].transform);
        LinesScript.MakeLine(new Vector3(0.36f, 0, -0.8441f), new Vector3(1.03f, 0, 1.005f), Color.blue, EmptyLevels[Level].transform);
        LinesScript.MakeLine(new Vector3(0.387f, 0, -0.8441f), new Vector3(1.03f, 0, 0.935f), Color.blue, EmptyLevels[Level].transform);
        LinesScript.MakeLine(new Vector3(0.416f, 0, -0.8441f), new Vector3(1.03f, 0, 0.87f), Color.blue, EmptyLevels[Level].transform);


    }
    public void Level7()           //Schieberegister Schritt 2
    {
        Level = 7;
        Checklist.changeText(Level);
        OnTrackingLost();
        OnTrackingFound();

        //PowerLine
        LinesScript.MakeLine(new Vector3(0.2455f, 0, -0.8441f), new Vector3(1.03f, 0, 1.2f), Color.red, EmptyLevels[Level].transform);

        //alle anderen LEDs (1-7)
        LinesScript.MakeLine(new Vector3(0.27f, 0, -1.1049f), new Vector3(0.949f, 0, -1.114f), Color.green, EmptyLevels[Level].transform);
        LinesScript.MakeLine(new Vector3(0.33f, 0, -1.134f), new Vector3(1.014f, 0, -1.114f), Color.green, EmptyLevels[Level].transform);
        LinesScript.MakeLine(new Vector3(0.36f, 0, -1.1589f), new Vector3(1.079f, 0, -1.114f), Color.red, EmptyLevels[Level].transform);

        //LinesScript.MakeLine(new Vector3(0.39f, 0, -1.1049f), new Vector3(1.144f, 0, -1.144f), Color.red, EmptyLevels[Level].transform);
        //LinesScript.MakeLine(new Vector3(0.43f, 0, -1.134f), new Vector3(1.209f, 0, -1.144f), Color.red, EmptyLevels[Level].transform);
        //LinesScript.MakeLine(new Vector3(0.46f, 0, -1.1589f), new Vector3(1.274f, 0, -1.144f), Color.red, EmptyLevels[Level].transform);

        //LinesScript.MakeLine(new Vector3(0.49f, 0, -1.1049f), new Vector3(1.339f, 0, -1.1716f), Color.red, EmptyLevels[Level].transform);

    }
}
