﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using UnityEngine.UI;

public class HandTracker : MonoBehaviour, ITrackableEventHandler
{

    protected TrackableBehaviour trackable;
    protected TrackableBehaviour.Status m_PreviousStatus;
    protected TrackableBehaviour.Status m_NewStatus;

    private bool mainTracked;
    private bool oTracked;

    public GameObject MainTracker;
    public GameObject ObjectTracker;

    public Text middleText;

    public bool handTracked = false;


    // Start is called before the first frame update
    void Start()
    {
        trackable = GetComponent<TrackableBehaviour>();
        if (trackable)
            trackable.RegisterTrackableEventHandler(this);



    }



    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        m_PreviousStatus = previousStatus;
        m_NewStatus = newStatus;


        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            TrackingFound();
        }
        else if (previousStatus == TrackableBehaviour.Status.TRACKED &&
                newStatus == TrackableBehaviour.Status.NO_POSE)
        {
            TrackingLost();
        }
        else
        {
            // For combo of previousStatus=UNKNOWN + newStatus=UNKNOWN|NOT_FOUND
            // Vuforia is starting, but tracking has not been lost or found yet
            // Call OnTrackingLost() to hide the augmentations
            TrackingLost();
        }


    }

    private void TrackingFound()
    {
        
        handTracked = true;

        var rendererComponents = trackable.GetComponentsInChildren<Renderer>(true);
        foreach (var component in rendererComponents)
            component.enabled = true;

        Debug.Log("Hand Found");
    }

    private void TrackingLost()
    {
        handTracked = false;

        var rendererComponents = trackable.GetComponentsInChildren<Renderer>(true);
        foreach (var component in rendererComponents)
            component.enabled = false;

        Debug.Log("Hand Lost");

    }

    // Update is called once per frame
    void Update()
    {
        mainTracked = MainTracker.GetComponent<MainTracker>().mainTracked;
        oTracked = ObjectTracker.GetComponent<ObjectTracker>().objectTracked;



        if (handTracked == true)
        {
            
                if (oTracked == true)
                {
                Vector3 position = this.transform.position;
                position[2] = position[2] * 0.3f; // Minimiere die Y Variable, da der Abstand mehr auf seitwärts ankommt.

                Vector3 dist = ObjectTracker.transform.position - position;

                middleText.GetComponent<Text>().text = dist.magnitude.ToString();
                }
            

           
            
        }
    }



}
