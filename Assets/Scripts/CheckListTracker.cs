﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using UnityEngine.UI;
using TMPro;

public class CheckListTracker : MonoBehaviour, ITrackableEventHandler
{

    protected TrackableBehaviour mTrackableBehaviour;
    protected TrackableBehaviour.Status m_PreviousStatus;
    protected TrackableBehaviour.Status m_NewStatus;

    public bool debug = false;

    public GameObject Seriallist;
    public GameObject Checklist;

    int gLevel;
    bool UpdateText;
    public BreadboardScript Breadboard;

    string[][] CheckListTextArray = new string[8][];

    TextMeshPro[] texts;

    public TextMeshPro t0;
    public TextMeshPro t1;
    public TextMeshPro t2;
    public TextMeshPro t3;
    public TextMeshPro t4;
    public TextMeshPro t5;
    public TextMeshPro t6;  
    public TextMeshPro t7;      //Überschrift



    // Start is called before the first frame update
    void Start()
    {
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
            mTrackableBehaviour.RegisterTrackableEventHandler(this);


        // Texte für Checkliste genereieren:
        texts = new TextMeshPro[] { t0, t1, t2, t3, t4, t5, t6, t7 };
        CheckListTextArray[0] = new string[7] { " Knöpfe für die Bedienung ", "- 3x Knopf", "- 3x kleine graue Kabel zu GND (-)", "- 3x Kabel zu PIN 6/7/8 ", " ", " " ," "};
        CheckListTextArray[1] = new string[7] { "- Button", "- Kleines Kabel zu GND", "- Kabel zu Pin 11", " ", " ", " ", " " };
        CheckListTextArray[2] = new string[7] { "- Widerstand 330Ohm verbinden mit GND", "- LED (kurzes Bein Zum Widerstand) ", "- Kabel zu PIN 7", " ", " ", " ", " " };
        CheckListTextArray[3] = new string[7] { "- Okker Kabel","- 3x Widerstand (2xlinks, 1x rechts", "- LED (langes Bein an Kabel) ", "- Kabel zu PIN 2/3/4 (R/G/B)", " ", " ", " " };
        CheckListTextArray[4] = new string[7] { "- 5V Kabel", "- GND Kabel", "- 5,1k Widerstand", "- Kabel PIN A8 ", "- Zu messener Widerstand ", " ", " " };
        CheckListTextArray[5] = new string[7] { "- Anstatt Widerstand kann auch", " ein Photosensor benutzt werden", "", " ", " ", " ", " Skip --) weiter " };
        CheckListTextArray[6] = new string[7] { "- Schieberegister (halbkreis links)", "- 8x LED (kurzes Bein zu Widerstand)  ", " 1x Kabel zu LED ", " ", " ", " ", " Skip --) weiter " };
        CheckListTextArray[7] = new string[7] { "- 7x Kabel zu LEDs  ", "- Power von Arduino ", "", " ", " ", " ", " " };
    }
    protected virtual void OnDestroy()
    {
        if (mTrackableBehaviour)
            mTrackableBehaviour.UnregisterTrackableEventHandler(this);
    }

    public void OnTrackableStateChanged(
        TrackableBehaviour.Status previousStatus,
        TrackableBehaviour.Status newStatus)
    {
        m_PreviousStatus = previousStatus;
        m_NewStatus = newStatus;

        Debug.Log("Trackable " + mTrackableBehaviour.TrackableName +
                  " " + mTrackableBehaviour.CurrentStatus +
                  " -- " + mTrackableBehaviour.CurrentStatusInfo);

        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED || newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED
            )
        {
            OnTrackingFound();
        }
        else if (previousStatus == TrackableBehaviour.Status.TRACKED &&
                 newStatus == TrackableBehaviour.Status.NO_POSE)
        {
            OnTrackingLost();
        }
        else
        {
            // For combo of previousStatus=UNKNOWN + newStatus=UNKNOWN|NOT_FOUND
            // Vuforia is starting, but tracking has not been lost or found yet
            // Call OnTrackingLost() to hide the augmentations
            OnTrackingLost();
        }
    }



    protected virtual void OnTrackingFound()
    {
        if (mTrackableBehaviour)
        {

            Renderer[] rendererComponents;

            if (debug == false)
            {
                rendererComponents = Checklist.GetComponentsInChildren<Renderer>(true);
            }
            else
            {
                rendererComponents = Seriallist.GetComponentsInChildren<Renderer>(true);
            }
            


            // Enable rendering:
            foreach (var component in rendererComponents)
                component.enabled = true;

        }
    }


    protected virtual void OnTrackingLost()
    {
        if (mTrackableBehaviour)
        {
            var rendererComponents = mTrackableBehaviour.GetComponentsInChildren<Renderer>(true);


            // Disable rendering:
            foreach (var component in rendererComponents)
                component.enabled = false;

        }
    }

    // Update is called once per frame
    void Update()
    {
        if (UpdateText == true)
        {
            for (int i = 0; i < 7; i++)
            {
                texts[i].text = CheckListTextArray[gLevel][i];
            }
            UpdateText = false;
        }
        
    }

    public void changeText(int Level)
    {

        gLevel = Level;
        UpdateText = true;

    }
}
